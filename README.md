# PyConnectors ![](pyconnectors/res/connector.svg)


[![version](https://img.shields.io/badge/version-4.0.0X-success.svg)](#)

## About
**PyConnectors** is a Python package for seamless querying, downloading, and uploading to and from the 
DISC database, as well as to *Oracle* databases. By abstracting away the boring connectivity part,
**PyConnectors** allows to write more legible, concise, and cleaner code,
letting you focus on what matters most. 

In addition, **PyConnectors** allows to run the same code both on CDSW as well as on ECB laptops.

The package can also be used from **R** via the R-library [*reticulate*](https://rstudio.github.io/reticulate/): 
have a look [here for more info](pyconnectors/docs/Access_from_R.md).

 [![](pyconnectors/res/book.svg) **Full documentation here**](pyconnectors/docs/UserGuide.md). 


### Currently supported databases
* DISC (and any database therein)
* All Oracle databases [listed here](pyconnectors/oracle_databases.py).


*If your favourite database is missing from the list, you can add it yourself: 
please refer to [this documentation](pyconnectors/docs/Oracle/how_to_connect.md), 
get in touch (luca.mingarelli@ecb.europa.eu), and/or send a pull request.*

### Requirements
* Anaconda Python 
* Git 
* Standard *Cloudera ODBC Driver for Impala* (you can check on Windows → ODBC Data Sources)

*Both can be requested on the IT portal.*

# Installation

## Locally
1. Open your Anaconda prompt
2. `XXX`

*Note: If you have issues installing have a look at the [troubleshoot page](pyconnectors/docs/FAQ.md).*

## From CDSW
1. CDSW Project → Start Workbench → Launch Python 3 Session → Start Terminal access
2. `XXX`

***Set hosts and pem certificate:***

```python
import pyconnectors
pyconnectors.set_hosts(IMPALA_HOST='<IMPALA-HOST>', 
          HTTPFS_HOST='<HTTPFS-HOST>', 
          HDFS_PATH='<HDFS-PATH>', 
          HIVE_HOST='<HIVE-HOST>')
pyconnectors.set_pem_certificate(certificate='loca/path/to/accprd-truststore.pem')
```

[comment]: # (2. `pip install git+ssh://git@bitbucket.ecb.de:7999/dgmf/connectors.git`)
***The following is necessary only if you want to connect to Oracle databases:***

(more details at the bottom of this page in the dedicated section)

3. Create new Environmental variables `CRP_USER`, `CRP_PWD`, `FSSDB_USER`, `FSSDB_PWD` and fill values accordingly 
(if you want to connect to CRP or FSSDB; for other Oracle database you can choose whichever name you prefer 
for the environmental variables storing user name and passwords).

If on CDSW:

4. CDSW Project Page → Settings → Engine → Environment Variables
5. Create new Environmental variable: 

    * Name: `LD_LIBRARY_PATH` 
    * Value: `/home/cdsw/.local/lib/python3.6/site-packages/pyoracleclient/instantclient:$LD_LIBRARY_PATH` 
![](pyconnectors/res/envvars.PNG)


*Note: you might have to restart your engine after installation.*

*Note2: If you have issues installing have a look at the [troubleshoot page](pyconnectors/docs/FAQ.md).*

# How to use 

For a full documentation and list of available functionalities 
please refer to the [User Guide](pyconnectors/docs/UserGuide.md).

For a quick introduction see below.

## DISC
### Queries
Performing specific queries and returning the results into *pandas dataframes* is easy:

```python
import pandas as pd
from pyconnectors import disc  # Opens connection to DISC

df = disc.read_sql(f"""SELECT DISTINCT(lei), hq_city
                     FROM crp_gleif.gleif_lei2_cdf_public_records
                     WHERE hq_city = 'London'
                    """)
                  
del disc  # Closes connection to DISC
```
### Navigate different databases
The following example illustrates how to navigate around different DISC
databases,and retrieve information on the stored tables

```python
from pyconnectors import disc  # Opens connection to DISC

disc.get_databases()
disc.select_database('crp_gleif')
disc.get_tables()
disc.describe_table('gleif_lei2_cdf_public_records')
```

### Input/Output
*PyConnectors* also allows to seamlessly read and write csv files from 
and to DISC.

```python
import pandas as pd
from pyconnectors import disc
df = pd.DataFrame({'A': [1,2,3],
                   'B': ['one', 'one', 'four']})
# Replace path with you username, or desired location
disc.to_csv(df=df, path='/user/mingare/table.csv')  
```
You should now be able to see your file saved at your chosen location:
```python
disc.listdir('/user/mingare', full_path=False)
```

and it is possible to read this file back with:

```python
df2 = disc.read_csv('/user/mingare/table.csv')
```

Notice that both `disc.to_csv` and `disc.read_csv` accept the standard *kwargs*
as *Pandas*' `to_csv` and `read_csv`:

```python
df2 = disc.read_csv('/user/mingare/table.csv', index_col=0)
```

Similarly in parquet or feather format:
```python
df = disc.read_parquet('/user/mingare/table.parq')
disc.to_parquet(df, '/user/mingare/table.parq')

df = disc.read_feather('/user/mingare/table.feather')
disc.to_feather(df, '/user/mingare/table.feather')
```
### More on connections:
The class DISC contains both an *impala* connection 
as well as a *pyarrow* or *WebHDFS* connection which can be accessed as
```python
disc._cnxn       # Impala connection
disc._hdfs_cnxn  # Pyarrow/WebHDFS connection
```
These can be used at will for more personalised access, 
storing in different formats, etc.

### Spark
**PyConnectors** also contains convenient methods to easily interface with spark.

```python
from pyconnectors import disc

disc.connect_spark(app_name='My new spark session')

disc.spark
```

The spark session is then stored as the attribute `disc.spark`.
At any point you can check the state of your connections by outputing
`disc`:

![](pyconnectors/res/discrepr.PNG)


The *Spark UI* can also be accessed via `disc.spark_ui`.



## CRP, FSSDB, and other *Oracle* databases

As long as *credentials* have been properly set (see *Installation* section above)
the *PyConnectors* package allows to query any Oracle database as well.
If on local machines, pass the relevant username and password as environmental variables:

```python
import os
os.environ['CRP_USER'] = 'your-crp-user'
os.environ['CRP_PWD'] = 'your-crp-pwd'
os.environ['FSSDB_USER'] = 'your-fssdb-user'
os.environ['FSSDB_PWD'] = 'your-fssdb-pwd'
```

As an example, consider the following block of code, querying from 
CRP's `company_mapping` table:

```python
from pyconnectors import crp

company_map = crp.read_sql("""SELECT CONAME, MKMVID, LEI, NAICS, NACE, COUNTRY_INC
                             FROM crp.company_mapping
                           """)
```

Analogously, to query from the FSSDB:

```python
from pyconnectors import fssdb

finrep_bs = fssdb.read_sql("""SELECT  NAME, RIAD_CODE, LEI, REPORTED_PERIOD, 
                                   SIGNINST, SSM_LIST, Y, VALUE_DECIMAL 
                           FROM sup.sup_general_s 
                           WHERE TABLE_ID = 'F_01.01'
                           """)
```

Access to the FSSDB and CRP are facilitated via two ad hoc classes. 
For any other [available](pyconnectors/oracle_databases.py) oracle database connect as:

```python
from pyconnectors import Oracle_Connector             

oc = Oracle_Connector(dsn=<YOUR_DSN>, # e.g. "CRP",
                      user=<YOUR_USERNAME>, 
                      pwd=<YOUR_PASSWORD>)
```

More info in the [documentation here](pyconnectors/docs/Oracle/how_to_connect.md).

# Author
Luca Mingarelli, 2020

[![Python](https://img.shields.io/static/v1?label=made%20with&message=Python&color=blue&style=for-the-badge&logo=Python&logoColor=white)](#)

