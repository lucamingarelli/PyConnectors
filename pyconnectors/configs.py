"""
Created on Wed Apr 7 19:01:21 2021

@author: Luca Mingarelli

"""
import os, shutil

def set_hosts(IMPALA_HOST, HTTPFS_HOST, HDFS_PATH, HIVE_HOST=None):
    """Sets hosts. Needs to be run only once after installation.
    Args:
        IMPALA_HOST (str):  IMPALA host (usually starts as impala.<...>)
        HTTPFS_HOST (str):  HTTPFS host (usually starts as httpfs.<...>)
        HDFS_PATH (str):  HDFS root path (usually starts as hdfs://<...>)
        HIVE_HOST (str):  HIVE host (usually starts as hive.<...>)
    """
    PATHS_FILE_PATH = os.path.join(os.path.dirname(__file__), '_PATHS.py')
    with open(PATHS_FILE_PATH, 'w+') as f:
        f.write(f"_IMPALA_HOST = '{IMPALA_HOST}'\n")
        f.write(f"_HIVE_HOST = '{HIVE_HOST}'\n")
        f.write(f"_HTTPFS_HOST = '{HTTPFS_HOST}'\n")
        f.write(f"_HDFS_PATH = '{HDFS_PATH}'\n")
        f.write("USER_GUIDE_URL = 'https://gitlab.sofa.dev/internal-open-source/data-and-connectivity-tools/Connectors/-/blob/master/connectors/docs/UserGuide.md'\n")


def set_pem_certificate(certificate):
    """Stores pem certificate (necessary to access via the Hadoop file system via WebHDFS.
    Needs to be run only once after installation.
    Args:
        certificate (str): local path to pem certificate file (with .pem extension)
    """
    __PEM_PATH = os.path.join(os.path.dirname(__file__), 'certificates/accprd-truststore.pem')
    shutil.copy(certificate, __PEM_PATH)