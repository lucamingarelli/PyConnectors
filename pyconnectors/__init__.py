from pyconnectors.__about__ import (__title__, __version__,
                                    __about__, __email__,
                                    __author__, __url__)
from pyconnectors._oracle_connector import Oracle_Connector
from pyconnectors.configs import *

try:
    from pyconnectors.disc import disc
except:
    pass

try:
    from pyconnectors.fssdb import fssdb
except:
    pass

try:
    from pyconnectors.crp import crp
except:
    pass
