# ![](pyconnectors/res/book.svg) User Guide

To quickly get started have a look at the [README file](README.md).

Further information on any specific method following
the links in the table of content below.

## Installation

Please refer to the [README file](README.md)
for instructions on how to install.

## DISC
You can start a connection by importing `disc` as

```python
from pyconnectors import disc
```
This will automatically open a connection to the *hdfs* as well as one
to *impala*. The object `disc` is created as an instance of the class `DISC`.

Should you prefer a *hive* connection, you can switch at any point as follows:

```python
from pyconnectors import disc
disc.open(hive=True)
```
Now in the object `disc` the impala connection has been replaced with a connection to *hive*.
Similarly, the command `disc.open(hive=False)` will get you back to an *impala* connection.

### Attributes
- [_cnxn](DISC/_cnxn.md)
- [_cursor](DISC/_cursor.md)
- [_hdfs_cnxn](DISC/_hdfs_cnxn.md)

### Methods available
- [get_databases](DISC/get_databases.md)
- [select_database](DISC/select_database.md)
- [get_tables](DISC/get_tables.md)
- [describe_table](DISC/describe_table.md)
- [create_table](DISC/create_table.md)
- [create_table_csv](DISC/create_table_csv.md)
- [_delete_table](DISC/_delete_table.md)
- [_fix_path](DISC/_fix_path.md)
- [listdir](DISC/listdir.md)
- [ls](DISC/ls.md)
- [read_sql](DISC/read_sql.md)
- [read_excel](DISC/read_excel.md)
- [read_csv](DISC/read_csv.md)
- [to_csv](DISC/to_csv.md)
- [read_parquet](DISC/read_parquet.md)
- [to_parquet](DISC/to_parquet.md)
- [read_feather](DISC/read_feather.md)
- [to_feather](DISC/to_feather.md)
- [read_stata](DISC/read_stata.md)
- [to_stata](DISC/to_stata.md)
- [to_pickle](DISC/to_pickle.md)
- [read_pickle](DISC/read_pickle.md)
- [read_encrypted](DISC/read_encrypted.md)
- [to_encrypted](DISC/to_encrypted.md)
- [open](DISC/open.md)
- [close](DISC/close.md)
- [upload_file](DISC/upload_file.md)
- [upload](DISC/upload.md)
- [savefig](DISC/savefig.md)
- [make_vintage](DISC/make_vintage.md)
- [hdfs_mv](DISC/hdfs_mv.md)
#### Spark attributes and methods
- [connect_spark](DISC/spark/connect_spark.md)
- [show_spark_conf](DISC/spark/show_spark_conf.md)
- [stop_spark](DISC/spark/stop_spark.md)
- spark_ui
- _spark_uri



## Oracle Databases

### Connection

- [How to connect](Oracle/how_to_connect.md)

### Attributes

- [_cnxn](Oracle/_cnxn.md)
- [_cursor](Oracle/_cursor.md)

### Methods available
      
- [get_tables](Oracle/get_tables.md)
- [describe_table](Oracle/describe_table.md)



# Access from R
- [How to access from R](Access_from_R.md)











