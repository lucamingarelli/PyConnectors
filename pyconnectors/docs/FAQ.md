[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


# Installations issues

If local installation does not go well please check 

1. To have `git` installed on your machine (available on the IT Portal)
2. To have set up `http.sslVerify` to `False`: you can do that via the command `git config --global http.sslVerify false`

Keep in mind that installation might take up to 4 minutes.

Sometimes pip might not be able to install some dependencies such as `fsspec` and `requests_kerberos`.
In case install them with conda, for example:
- `conda install fsspec`
- `conda install -c anaconda requests-kerberos`
- `conda install pyarrow`
- etc.

Then do again `pip install git+https://bitbucket.ecb.de/scm/dgmf/connectors`.

If you still have issues, it might be because you have received a faulty
python installation from IT - in this case you won't be able to install
any package via `pip`. 
To solve this problem:

1. Create folder `C:\Users\<username>\AppData\Roaming\pip` (if it doesn't exist already)
2. Copy the `pip.ini` file from the `\\EUC\EUC-Sources$\DAS-DISC\Anaconda\Anaconda3-2019.03\Source` to `C:\Users\<username>\AppData\Roaming\pip`


# DISC

If you have issues reading *snappy* compressed files, make sure you have `python-snappy` installed.
If not run `conda install python-snappy`.

# Uninstall
Uninstall as 

`pip uninstall ecb-connectors`