[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.Oracle_Connector.**_cnxn**

Oracle connection to the DISC HDFS filesystem (Hadoop file system).

#### Examples:

```python
import pandas as pd
from pyconnectors import crp  # Opens connection to CRP

company_map = pd.read_sql("""SELECT CONAME, MKMVID, LEI, NAICS, NACE, COUNTRY_INC
                             FROM crp.company_mapping
                          """, con=crp._cnxn)
                  
del crp  # Closes connection to CRP
```