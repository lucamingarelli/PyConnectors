[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.Oracle_Connector.**_cursor**

Cursor object which can be used to perform queries or navigate the database.

#### Examples:

```python
from pyconnectors import crp  # Opens connection to CRP

crp._cursor.execute("SELECT table_name FROM all_tables")
tables, = zip(*crp._cursor.fetchall())
print(tables)

del crp  # Closes connection to CRP
```
    