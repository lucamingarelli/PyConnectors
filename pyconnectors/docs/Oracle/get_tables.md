[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.Oracle_Connector.**get_tables**(self, ret=True)

Shows tables in database. 

### Parameters

**ret** (*bool*): If `False` simply prints out the tables' names.

### Returns:
List of tables in database.

#### Examples:

```python
from pyconnectors import crp  # Opens connection to CRP

disc.get_tables(ret=False)

del crp  # Closes connection to CRP
```