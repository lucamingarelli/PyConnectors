[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.Oracle_Connector.**describe_table**(self, table)

Describes a table and returns DataFrame with description.
Outputs dataframe with column names, data type, and associated description.

### Parameters:

**table** (*str*): table to be described.

### Returns:

Dataframe.

#### Examples:

```python
from pyconnectors import crp  # Opens connection to DISC

tbls = crp.get_tables(ret=True)
crp.describe_table('COMPANY_CONTENT')

del crp  # Closes connection to FSSDB
```