[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)

# Oracle databases: How to connect
You can in general connect to any oracle database listed [here](connectors/oracle_databases.py) as:

```python
from pyconnectors import Oracle_Connector             

oc = Oracle_Connector(dsn=<YOUR_DSN>, 
                      user=<YOUR_USERNAME>, 
                      pwd=<YOUR_PASSWORD>)

```
If you are working on CDSW it is good practice to set your credentials 
as environmental variables and keep your project private.

If you are in doubt as to what's your DSN, you can simply find this out 
at `Start -> Data Sources (ODBC)` if you are on your local (Windows) laptop. 
On CDSW you can check the attribute `connectors.oracle_databases.ORACLE_DBS` 
to check which databases are supported by default.
If your database is not listed here you can add it via the package [`pyoracleclient`](https://github.com/LucaMingarelli/PyOracleClient)
(it is already installed automatically with `connectors`). 
See at the bottom of this page to learn how to add new databases.

## Simplified access to CRP and FSSDB
For CRP and FSSDB simplified access is provided.
In order to connect from CDSW all you need to do is

```python
from pyconnectors import crp      # Opens connection to CRP
# and
from pyconnectors import fssdb  # Opens connection to FSSDB
```

From local machines, the following will work provided your corresponding DNSs are
called `CRP`, and `FSSDB` respectively:

```python
import os
os.environ['CRP_USER'] = <YOUR_USERNAME>
os.environ['CRP_PWD']  = <YOUR_PASSWORD>
from pyconnectors import crp      # Opens connection to CRP
# and
os.environ['FSSDB_USER'] = <YOUR_USERNAME>
os.environ['FSSDB_PWD']  = <YOUR_PASSWORD>
from pyconnectors import fssdb  # Opens connection to FSSDB
```

If this was not the case you can still override the default DNS 
and connect as

```python
from pyconnectors._oracle_connector import Oracle_Connector             

oc = Oracle_Connector(dsn=<YOUR_DSN>, # if not "CRP"
                      user=<YOUR_USERNAME>, 
                      pwd=<YOUR_PASSWORD>)

```
or change the DSN in `Start -> Data Sources (ODBC)`.


## Register new databases

Register your database as follows:

```python
import pyoracleclient as pyoc

pyoc.add_tns(name='FSSDB', service_name='servicename.prd.tns',
             protocol1='TCP', host1=<your-address1>, port1=<your-port>,
             protocol2='TCP', host2=<your-address2>, port2=<your-port>)
```

This needs to be done only once.

Alternatively you can append your own tns specification as a string as follows:

```python
import pyoracleclient as pyoc

pyoc.add_custom_tns("""yourtnsname = (DESCRIPTION=
                    (ADDRESS_LIST=(FAILOVER=ON)
                                  (LOAD_BALANCE = OFF)
                                  (ADDRESS=(PROTOCOL=TCP)
                                           (HOST=host1address)
                                           (PORT=1000))
                                  (ADDRESS=(PROTOCOL=TCP)
                                           (HOST=host1address)
                                           (PORT=1000)))
                                  (CONNECT_DATA=(SERVER=dedicated)
                                  (SERVICE_NAME=servicename.prd.tns)))
                    """)
```

If you think others might need access to this database please send a merge request to the Connectors project 
or simply request this database to be added.



