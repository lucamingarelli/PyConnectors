[Back to Main Page](../../README.md)

[Back to User Guide](UserGuide.md)


# Access from R

The *reticulate* library allows you to leverage the power of python in R.

If *reticulate* is not already installed, do so as `install.packages('reticulate')`

Once you have **Connectors**  installed in python, you can then
access its functions from R:

```r
library(reticulate)

Sys.setenv(RETICULATE_PYTHON = 'C:/Anaconda3/python.exe')

# Or your python location: e.g. from the conda environment BICG
# do instead:
# Sys.setenv(RETICULATE_PYTHON = 'C:/Anaconda3/envs/BICG/python.exe')

connectors <- import("connectors")
disc <- connectors$disc

disc$listdir('/user/mingare')

df <- disc$read_parquet('/user/mingare/table.parq')

disc$to_parquet(df, '/user/mingare/new_table.parq')

# etc.
```

It is important you select the python version on which 
you have installed *Connectors*. This is done on line 2 above, by setting the 
environmental variable `RETICULATE_PYTHON`. The standard on your ECB laptop
should be `C:/Anaconda3/python.exe`. Should not this be the case you can always
find out by opening up a terminal (e.g. your Anaconda prompt), 
and entering `where python`. Most likely your other installations will reside
in other conda environments you have created, e.g. at
`C:/Anaconda3/envs/<your-env>/python.exe`.

On **CDSW** instead you should set `RETICULATE_PYTHON='/usr/local/bin/python'`.

**Note**: The first time you might be asked whether you want to install Miniconda: 
simply reply `n` to the prompt.




