[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**close**(self, rm_local_temp=False)
Closes DISC connection.

### Parameters:

**rm_local_temp** (*bool*): Delete local temp folder. Default is False.



#### Examples:

```python
from pyconnectors import disc  # Opens connection to DISC

disc.close()  # Closes connection to DISC
disc.open()   # Connection to disc reopened

del disc      # Closes connection to DISC and deleted disc object.
```