[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**get_databases**(self, ret=False)

Prints and returns available databases.

### Parameters:

**ret** (*bool*): whether to return (`True`) or print (`False`) the list of available databases.

#### Examples:

```python
from pyconnectors import disc  # Opens connection to DISC

disc.get_databases()
disc.select_database('crp_gleif')
disc.get_tables(ret=False)
disc.describe_table('gleif_lei2_cdf_public_records')

del disc  # Closes connection to DISC
```

