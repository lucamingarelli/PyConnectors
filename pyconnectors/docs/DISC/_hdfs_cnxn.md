[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)



## connectors.disc.**_hdfs_cnxn**

Connection to the DISC HDFS filesystem (Hadoop file system): 
this is either a `pyarrow` connection, or a `WebHDFS` connection 
depending on whether you are working on CDSW or locally. 

It is possible to check that via the attribute `type`:
`disc._hdfs_cnxn.type` will output either `PyArrow` or `WebHDFS`.

***NOTE***: functionalities may vary depending on which platform you are working from.


#### Some methods:


```python

disc._hdfs_cnxn.exists('/user/mingare/table.csv')                                       # Check existence of file or directory
                            
disc._hdfs_cnxn.isdir('/user/mingare')                                                  # Check if is directory
                           
disc._hdfs_cnxn.isfile('/user/mingare')                                                 # Check if is file
                           
disc._hdfs_cnxn.info('/user/mingare')                                                   # Info on path
                           
disc._hdfs_cnxn.ls('/user/mingare')                                                     # Lists content of directory
                           
disc._hdfs_cnxn.mkdir('/user/mingare/new_directory')                                    # Creates new directory

disc._hdfs_cnxn.mv('/user/mingare/table.csv', '/user/mingare/new_directory/table.csv')  # Moves file from source location to destination

disc._hdfs_cnxn.rm('/user/mingare/new_directory', recursive=True)                       # WARNING: Removes directory and all its content!

...

```


