[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**read_sql**(self, query, **kwargs)
Performs a SQL query on DISC.

### Parameters:

**query** (*str*): a SQL query. 

****kwargs**: Keyword arguments to be passed to [pandas.read_sql](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_sql.html).



#### Examples:

```python
import pandas as pd
from pyconnectors import disc  # Opens connection to DISC


df = disc.read_sql(f"""SELECT DISTINCT(lei), hq_city
                     FROM crp_gleif.gleif_lei2_cdf_public_records
                     WHERE hq_city = 'London'
                    """)
                  
del disc  # Closes connection to DISC
```