[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**hdfs_mv**(self, origin_path, destination_path)

Moves files and folders across different locations in the HDFS.

### Parameters:

**origin_path** (*str*): HDFS file or directory to be transfered. 

**destination_path** (*str*): HDFS destination.



#### Examples:

```python
from pyconnectors import disc  # Opens connection to DISC

disc.hdfs_mv('/user/mingare/file.csv', '/user/mingare/folder')
                  
del disc  # Closes connection to DISC
```