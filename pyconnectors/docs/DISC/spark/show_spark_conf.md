[Back to Main Page](../../../../README.md)

[Back to User Guide](../../UserGuide.md)


## connectors.disc.**show_spark_conf**(self)

Displays spark configurations. 


#### Examples:

```python
from pyconnectors import disc  # Opens connection to DISC

disc.connect_spark(app_name='My new session')

disc.show_spark_conf()

del disc  # Closes connection to DISC
```
    