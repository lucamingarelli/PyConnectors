[Back to Main Page](../../../../README.md)

[Back to User Guide](../../UserGuide.md)


## connectors.disc.**connect_spark**(self, app_name=None, master=None, config=dict(), return_SparkSession=False)

Connects to spark via pyspark. Stores the spark session in the attribute `disc.spark`. 
Access to the Spark UI is provided via the link `disc.spark_ui` 
(the address is also available as `disc._spark_uri)`. 

### Parameters:

**app_name** (*str*): A name for the current session.

**master**  (*str*): Either `yarn` (default) or `local`.                   

**config**  (*dict*): Dictionary with spark configurations.     

**return_SparkSession**  (*bool*):  Whether to return the spark session (default is False).


#### Examples:

```python
from pyconnectors import disc  # Opens connection to DISC

disc.connect_spark(app_name='My new session')

del disc  # Closes connection to DISC
```
    