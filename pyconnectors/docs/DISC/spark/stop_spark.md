[Back to Main Page](../../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**stop_spark**(self)

Stops spark if spark is connected. 

#### Examples:

```python
from pyconnectors import disc  # Opens connection to DISC

disc.connect_spark(app_name='My new session')

disc.stop_spark()

del disc  # Closes connection to DISC
```
    