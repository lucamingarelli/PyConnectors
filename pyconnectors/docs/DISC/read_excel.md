[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**read_excel**(self, path, **kwargs)
Read an Excel (xls, xlsx, xlsm, xlsb, odf, ods and odt) file into DataFrame.

### Parameters:

**path** (*str*): path to disc storage location. 

****kwargs**: Keyword arguments to be passed to [pandas.read_excel](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_excel.html).

### Returns: 
**Dataframe**

#### Examples:

```python
from pyconnectors import disc  # Opens connection to DISC

df = disc.read_excel('/user/mingare/table.xlsx')
                  
del disc  # Closes connection to DISC
```