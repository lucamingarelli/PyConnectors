[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**to_encrypted**(self, df, path, password, permissions='777')
Write a DataFrame as encrypted binary at specified DISC location.

### Parameters:

**df** (*pandas.DataFrame*): A pandas dataframe to be stored.

**path** (*str*): path to disc storage location. 

**password** (*str*): Password for encryption.

**permissions**  (*str* or None): Posix string for permissions: [see here to learn more](https://en.wikipedia.org/wiki/File-system_permissions). 


#### Examples:

```python
import pandas as pd
from pyconnectors import disc  # Opens connection to DISC

df = pd.DataFrame(data={'col1': [1, 2], 'col2': [3, 4]})

disc.to_encrypted(df, '/user/mingare/table.encrypted', password='my-password')
disc.read_encrypted('/user/mingare/table.encrypted', password='my-password')
                  
del disc  # Closes connection to DISC
```