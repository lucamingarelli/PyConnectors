[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**read_csv**(self, path, name=None, **kwargs)
Read a comma-separated values (csv) file into DataFrame.

### Parameters:

**path** (*str*): path to disc storage location. 

**name** (*str*): name of file. Included for backward compatibility. 

****kwargs**: Keyword arguments to be passed to [pandas.read_csv](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_csv.html).

### Returns: 
**Dataframe**

#### Examples:

```python
from pyconnectors import disc  # Opens connection to DISC

df = disc.read_csv('/user/mingare/table.csv')

#Alternatively
#df = disc.read_csv(path='/user/mingare/', name='table.csv')
                  
del disc  # Closes connection to DISC
```