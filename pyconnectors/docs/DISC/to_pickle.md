[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**to_pickle**(self, obj, path, protocol='HIGHEST_PROTOCOL', permissions='777', **kwargs)
Write an object `obj` to serialised binary format at DISC location.

### Parameters:

**obj**: Object to be serialised.

**path** (*str*): path to disc storage location. 

**protocol** (*str* or *int*): Pickle protocol: either a string (`HIGHEST_PROTOCOL` or `DEFAULT_PROTOCOL`) or an integer.

**permissions**  (*str* or None): Posix string for permissions: [see here to learn more](https://en.wikipedia.org/wiki/File-system_permissions). 

****kwargs**: Keyword arguments to be passed to [pickle.dump](https://docs.python.org/3/library/pickle.html).


#### Examples:

```python
import pandas as pd
from pyconnectors import disc  # Opens connection to DISC

df = pd.DataFrame(data={'col1': [1, 2], 'col2': [3, 4]})

disc.to_pickle(df, '/user/mingare/table.p')
df2 = disc.read_pickle('/user/mingare/table.p')
                  
del disc  # Closes connection to DISC
```