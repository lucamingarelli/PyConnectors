[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**create_table**(self, df, lab, table_name, path, external=True, permissions='777')

Deletes previous table, stores dataframe as parquet,
creates new table, and refreshes it.

### Parameters:

**df** (*pandas.DataFrame* or *pyspark.sql.dataframe.DataFrame*): dataframe to be converted into table.

**lab**  (*str*): datalab where to host the table (e.g. `lab_dlb_disc_srf`).                   

**table_name**  (*str*): name of the new table.

**path**  (*str*): path. 

**external**  (*bool*): Whether to make an `EXTERNAL` table or a managed one. 

**permissions**  (*str*): Posix string for permissions: [see here to learn more](https://en.wikipedia.org/wiki/File-system_permissions). 


Example:

```python
import pandas as pd
from pyconnectors import disc

df = pd.DataFrame({'A': [1,2,3],
                   'B': ['one', 'one', 'four']})
                   
disc.create_table(df, lab='lab_dlb_ecb_public', 
                      table_name='my_new_table',
                      path='/data/lab/dlb_ecb_public/db/dir_for_my_table')  # It is good practice to create a folder to store data for your table! 
```

You can also create views as:

```python
disc._cursor.execute("""CREATE VIEW lab_dlb_ecb_public.my_new_view AS
                        SELECT a, b
                        FROM lab_dlb_ecb_public.my_new_table""")

```