[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**upload_file**(self, local_file, destination_file_path, rm_local, overwrite, permissions='777')

Transfers file `local_file` to remote hdfs destination `destination_file_path`.
If `rm_local==True` local file is deleted.

### Parameters:

**local_file** (*str*): local file to be transfered.

**destination_file_path**  (*str*): remote destination (full path).                   

**rm_local**  (*bool*): whether to remove local file (default: `False`).

**overwrite** (*bool*): whether to overwrite file at destination (default: `True`)

**permissions**  (*str* or None): Posix string for permissions: [see here to learn more](https://en.wikipedia.org/wiki/File-system_permissions). 


#### Example:

```python
from pyconnectors import disc  # Opens connection to DISC

disc.upload('my_local_file.dat',
            '/user/mingare/copy_of_my_local_file_on_disc.dat',
            rm_local=True)  

del disc      # Closes connection to DISC.
```

