[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**create_table_csv**(self, df, lab, table_name, cols, dtypes, path)

Deletes previous table, transfers new csv file,
creates new table, and refreshes it.

### Parameters:

**df** (*pandas.DataFrame*): dataframe to be converted into table.

**lab**  (*str*): datalab where to host the table.                   

**table_name**  (*str*): name of the new table.

**cols**  (*list of str*): name of the columns to be associated with the new table.

**dtypes**  (*list of types*): data types of each column in the new table.

**path**  (*str*): path. 




