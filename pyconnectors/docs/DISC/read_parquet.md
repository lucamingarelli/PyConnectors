[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**read_parquet**(self, path, **kwargs)
Load a parquet object from the file path, returning a DataFrame.

### Parameters:

**path** (*str*): path to disc storage location. 

****kwargs**: Keyword arguments to be passed to [pandas.read_parquet](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_parquet.html).

### Returns: 
**Dataframe**

#### Examples:

```python
from pyconnectors import disc  # Opens connection to DISC

df = disc.read_parquet('/user/mingare/table.parq')
                  
del disc  # Closes connection to DISC
```