[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**ls**(self, path)

Return a list containing the names of the entries in the directory given by path. 
The list is in arbitrary order, and does not include the special entries '.' and '..' even if they are present in the directory.

### Parameters:


**path** (*str*): path to disc storage location. 

### Returns:
List of strings of content of provided `path`.

#### Examples:

```python
from pyconnectors import disc  # Opens connection to DISC

disc.ls('/user/mingare')
                  
del disc  # Closes connection to DISC
```

*See also [`disc.listdir`](./listdir.md)*.