[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**select_database**(self, database)

Navigates to given `database`. 
The selected database name is stored in `self.db`.

### Parameters:

**database** (*str*): the database to be selected.

#### Examples:

```python
from pyconnectors import disc  # Opens connection to DISC

disc.get_databases()
disc.select_database('crp_gleif')
disc.get_tables(ret=False)
disc.describe_table('gleif_lei2_cdf_public_records')

del disc  # Closes connection to DISC
```