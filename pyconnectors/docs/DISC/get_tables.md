[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)

## connectors.disc.**get_tables**(self, database=None, ret=True)

Shows tables in currently selected database. 
A database must be already selected prior the call to `get_tables` (see example below),
or specified via the argument `database`.


### Parameters:

**database** (*str*): Default `None`; in this case selecting from currently selected database (self.db). Otherwise allows to select database.  

**ret** (*bool*): If `False` simply prints out the tables' names.

### Returns:
List of tables in currently selected database.

#### Examples:

```python
from pyconnectors import disc  # Opens connection to DISC

disc.get_databases()
disc.select_database('crp_gleif')
disc.get_tables(ret=False)
disc.describe_table('gleif_lei2_cdf_public_records')

del disc  # Closes connection to DISC
```