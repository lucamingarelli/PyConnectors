[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**make_vintage**(self, origin, freq='month', overwrite=False, deep=False, exclude=[])

A tool to create data vintages. 
If does not exist yet, creates a new folder `origin/VINTAGES` within which it creates
vintages subfolders depending on the frequency `freq` chosen. 

For the default `freq='month'`, a folder `origin/VINTAGES/<current-year>/<current-month>` 
is created, within which all content of `origin` is stored 
(exclusion made for the folder `VINTAGES` itself).

Whenever the line `disc.make_vintage(...)` is encountered a vintage is made if not already existing at the chosen frequency.
As an example, if `freq` is set to `month` and one vintage has already been created this month, the function will either 
overwrite the current vintage (if `overwrite=True`) or return an error (if `overwrite=False`). 

The frequency `max` corresponds to a resolution of one second. That is, a new vintage is made at every new run.

### Parameters:


**origin** (*str*): The folder of which you want to store vintages. A new `origin/VINTAGES` folder will be created.

**freq** (*str*): Frequency with which to store vintages. Choose between `year`, `month`, `day`, `max`. Default is `month`.

**overwrite** (*bool*): Whether to overwrite already existing vintages. Default is `False`.

**deep** (*bool*): If True stores as year/month/etc. Else in a single folder year_month_day_etc.

**exclude** (*list*): List of items (files or directories) in origin, of which no vintaging should occur.

#### Examples:

```python
from pyconnectors import disc  # Opens connection to DISC

disc.make_vintage(origin='/user/mingare')
                  
del disc  # Closes connection to DISC
```


