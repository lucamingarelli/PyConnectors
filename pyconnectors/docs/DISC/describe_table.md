[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**describe_table**(self, table)

Describes a table and returns DataFrame with description.
Outputs dataframe with column names, data type, and associated description.

### Parameters:

**table** (*str*): table to be described.

### Returns:

Dataframe.

#### Examples:

```python
from pyconnectors import disc  # Opens connection to DISC

disc.get_databases()
disc.select_database('crp_gleif')
disc.get_tables(ret=False)
disc.describe_table('gleif_lei2_cdf_public_records')

del disc  # Closes connection to DISC
```