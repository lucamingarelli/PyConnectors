[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**listdir**(self, path, full_path=True)

Return a list containing the names of the entries in the directory given by path. 
The list is in arbitrary order, and does not include the special entries '.' and '..' even if they are present in the directory.

### Parameters:


**path** (*str*): path to disc storage location. 

**full_path** (*bool*): Whether to return full path of each files (`True`) or simply file names (`False`).

### Returns:
List of strings containing either full paths or file names.

#### Examples:

```python
from pyconnectors import disc  # Opens connection to DISC

disc.listdir('/user/mingare')
                  
del disc  # Closes connection to DISC
```

*See also [`disc.ls`](./ls.md)*.