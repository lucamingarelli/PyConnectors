[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**read_stata**(self, path, **kwargs)
Read a DataFrame to the stata dta format at DISC location.

### Parameters:

**path** (*str*): path to disc storage location. 

****kwargs**: Keyword arguments to be passed to [pandas.DataFrame.read_stata](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_stata.html).

### Returns: 
**Dataframe**

#### Examples:

```python
import pandas as pd
from pyconnectors import disc  # Opens connection to DISC

df = pd.DataFrame(data={'col1': [1, 2], 'col2': [3, 4]})

disc.to_stata(df, '/user/mingare/table.dta')
disc.read_stata('/user/mingare/table.dta')
                  
del disc  # Closes connection to DISC
```