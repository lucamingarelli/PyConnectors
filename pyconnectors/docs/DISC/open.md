[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**open**(self, hive=False)
Opens DISC connection: 
selects automatically according to platform (Local Windows or CDSW)

### Parameters:

**hive** (*bool*): Whether to return an *impala* (`False`) or *hive* (`True`) connection.

#### Examples:

```python
from pyconnectors import disc  # Opens connection to DISC

disc.close()  # Closes connection to DISC
disc.open()   # Connection to disc reopened

del disc      # Closes connection to DISC and deletes disc object.
```