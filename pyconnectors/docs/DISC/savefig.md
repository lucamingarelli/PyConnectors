[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**savefig**(self, path, ax=None, **kwargs)
Saves matplotlib figure as `.png` at DISC location.

### Parameters:

**path** (*str*): path to disc storage location. 

**ax** (*matplotlib.axes*): save from specific matplotlib axes instead.

****kwargs**: Keyword arguments to be passed to [matplotlib.pyplot.savefig](https://matplotlib.org/3.3.0/api/_as_gen/matplotlib.pyplot.savefig.html).



#### Examples:

```python
import numpy as np
from pyconnectors import disc  # Opens connection to DISC

def my_plot():
    x = np.linspace(0,6,50)
    y = np.sin(x)
    plt.plot(x, y)
    disc.savefig('/user/mingare/myfig.png', dpi=100)

my_plot()
                  
del disc  # Closes connection to DISC
```