[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**makedir**(self,destination_path)

Wrapper around WebHDFS.makedir (windows - also accessible as self._hdfx_cnxn.makedir)
and pyarrow.mkdir (unix - also accessible as self._hdfx_cnxn.mkdir).

### Parameters:


**destination_path** (*str*): Path to be created.


#### Examples:

```python
from pyconnectors import disc  # Opens connection to DISC

disc.makedir('/user/mingare/my_new_dir/and_sub_dir')
                  
del disc  # Closes connection to DISC
```
