[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**_cnxn**

Impala connection to the DISC filesystem (Hadoop file system).

#### Examples:

```python
import pandas as pd
from pyconnectors import disc  # Opens connection to DISC

df = pd.read_sql(f"""SELECT DISTINCT(lei), hq_city
                     FROM crp_gleif.gleif_lei2_cdf_public_records
                     WHERE hq_city = 'London'
                  """, con=disc._cnxn)
                  
del disc  # Closes connection to DISC
```