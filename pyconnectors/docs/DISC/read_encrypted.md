[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**read_encrypted**(self, path, password)
Reads an encrypted DataFrame from DISC location.

### Parameters:

**path** (*str*): path to disc storage location. 

**password** (*str*): Password for encryption.


#### Examples:

```python
import pandas as pd
from pyconnectors import disc  # Opens connection to DISC

df = pd.DataFrame(data={'col1': [1, 2], 'col2': [3, 4]})

disc.to_encrypted(df, '/user/mingare/table.encrypted', password='my-password')
disc.read_encrypted('/user/mingare/table.encrypted', password='my-password')
                  
del disc  # Closes connection to DISC
```