[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**upload**(self, local_path, destination_path)

Uploads files and/or folders from `local_path` onto the DISC `destination_path`

### Parameters:

**local_path** (*str*): local file or directory to be transfered.

**destination_path**  (*str*): remote destination (full path).                   



#### Example:

```python
from pyconnectors import disc  # Opens connection to DISC

disc.upload('my_local_folder',
            '/user/mingare/')  

del disc      # Closes connection to DISC.
```

