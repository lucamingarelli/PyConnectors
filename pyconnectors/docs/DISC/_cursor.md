[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**_cursor**

Cursor object which can be used to perform queries or navigate the database.

#### Examples:

```python
from pyconnectors import disc  # Opens connection to DISC

disc._cursor
# is the same as 
# cursor = disc._cnxn.cursor()

disc._cursor.execute("show databases")
print(disc._cursor.fetchall())

del disc  # Closes connection to DISC
```
    