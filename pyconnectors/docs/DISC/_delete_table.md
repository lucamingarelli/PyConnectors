[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**_delete_table**(self, table)

Deletes `table` if exists in currently selected database (`self.db`).

### Parameters:

**table**  (*str*): name of the table to be deleted.
