[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**read_feather**(self, path, **kwargs)
Read a DataFrame to the binary feather format at DISC location.

### Parameters:

**path** (*str*): path to disc storage location. 

****kwargs**: Keyword arguments to be passed to [pandas.DataFrame.read_feather](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_feather.html).

### Returns: 
**Dataframe**

#### Examples:

```python
import pandas as pd
from pyconnectors import disc  # Opens connection to DISC

df = pd.DataFrame(data={'col1': [1, 2], 'col2': [3, 4]})

disc.to_feather(df, '/user/mingare/table.feather')
disc.read_feather('/user/mingare/table.feather')
                  
del disc  # Closes connection to DISC
```