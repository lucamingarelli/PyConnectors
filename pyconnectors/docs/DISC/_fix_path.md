[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**_fix_path**(self, path)

Adds the starting Hadoop file system address in `_HDFS_PATH` 
at the beginning of the path.

### Parameters:

**path** (*str*): path to disc storage location. 

### Returns:
Corrected path.

#### Examples:

```python
from pyconnectors import disc  # Opens connection to DISC

full_path = disc._fix_path('/user/mingare')
                  
del disc  # Closes connection to DISC
```



    