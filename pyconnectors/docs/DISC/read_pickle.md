[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**read_pickle**(self, path, **kwargs)
Read a serialised object from DISC location.

### Parameters:

**path** (*str*): path to disc storage location. 

****kwargs**: Keyword arguments to be passed to [pickle.load](https://docs.python.org/3/library/pickle.html).



#### Examples:

```python
import pandas as pd
from pyconnectors import disc  # Opens connection to DISC

df = pd.DataFrame(data={'col1': [1, 2], 'col2': [3, 4]})

disc.to_pickle(df, '/user/mingare/table.p')
df2 = disc.read_pickle('/user/mingare/table.p')
                  
del disc  # Closes connection to DISC
```