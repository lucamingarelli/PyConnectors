[Back to Main Page](../../../README.md)

[Back to User Guide](../UserGuide.md)


## connectors.disc.**to_csv**(self, df, path, permissions='777', **kwargs)
Write a DataFrame to comma separated format at DISC location.

### Parameters:

**df** (*pandas.DataFrame*): A pandas dataframe to be stored.

**path** (*str*): path to disc storage location. 

**permissions**  (*str* or None): Posix string for permissions: [see here to learn more](https://en.wikipedia.org/wiki/File-system_permissions). 

****kwargs**: Keyword arguments to be passed to [pandas.DataFrame.to_csv](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.to_csv.html).



#### Examples:

```python
import pandas as pd
from pyconnectors import disc  # Opens connection to DISC

df = pd.DataFrame(data={'col1': [1, 2], 'col2': [3, 4]})

disc.to_csv(df, '/user/mingare/table.csv')
disc.read_csv('/user/mingare/table.csv')
                  
del disc  # Closes connection to DISC
```