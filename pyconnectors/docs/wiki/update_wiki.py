import os, sys, shutil
from distutils.dir_util import copy_tree


_WIKI = '_WIKI'
os.mkdir(_WIKI)


for src in ['docs/DISC', 'docs/Oracle']:
    copy_tree(f'connectors/{src}', 
              f"./{_WIKI}/{src.split('/')[-1]}")
    
for src in ['Access_from_R.md', 'FAQ.md', 'UserGuide.md']:
    shutil.copy2(f'connectors/docs/{src}', 
                 f"./{_WIKI}/{src.split('/')[-1]}")
    
shutil.copy2('./README.md', f'./{_WIKI}/Connectors.md')


