__title__ = "pyconnectors"
__about__ = "A set utilities to seamlessly connect to DISC and other Oracle databases from CDSW or Local ECB Machines."
__version__= '4.0.0X'
__author__ = 'Luca Mingarelli'
__email__ = "luca.mingarelli@ecb.europa.eu"
__url__ = "https://bitbucket.ecb.de/projects/DGMF/repos/connectors" 