"""
Created on Tue Mar 17 00:11:21 2020

@author: mingare

"""
import os, functools
from pyconnectors._oracle_connector import Oracle_Connector

# Define FSSDB Connector    
FSSDB = functools.partial(Oracle_Connector, dsn="FSSDB")

try:
    fssdb = FSSDB(user=os.environ['FSSDB_USER'], 
                  pwd=os.environ['FSSDB_PWD']) 
except:
    raise Warning("""Incorrect credentials. 
    Please either set the correct credentials in the environmental variables
    
    user=os.environ['FSSDB_USER']
    pwd=os.environ['FSSDB_PWD']
    
    or instantiate as 

    fssdb = FSSDB(user=<your-user>, pwd=<your-pwd>)""")
