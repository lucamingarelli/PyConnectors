PORTS = [1521, 1521]
HOSTS = ['xp01prd-scan.ecb.de', 'xp02prd-scan.ecb.de']

ORACLE_DBS = {'FSSDB':'pfss_svc.prd.tns',
              'CRP':'pcrp_svc.prd.tns',
#              'AnaCredit': 'pacr_svc.prd.tns', 
              #SBIREPO
              'psbirep_svc':'psbirep_svc.prd.tns',
              'psbirep_adg':'psbirep_adg.prd.tns',
              ##RAR and ODB ODS
              'SBIODS': 'psbiods_svc.prd.tns',
              'psbiods_svc': 'psbiods_svc.prd.tns',
              'psbiods_adg': 'psbiods_adg.prd.tns',
              ##RAR DWH
              'SBIDW': 'prardw_svc.prd.tns',
              'prardw_svc': 'prardw_svc.prd.tns',
              'prardw_adg': 'prardw_adg.prd.tns',
              ##RAR DM
              'prardm_svc': 'prardm_svc.prd.tns',
              'prardm_adg': 'prardm_adg.prd.tns',
              ##ODB DWH
              'podbdw_svc': 'podbdw_svc.prd.tns',
              'podbdw_adg': 'podbdw_adg.prd.tns',
              ##ODB DM
              'podbdm_svc': 'podbdm_svc.prd.tns',
              'podbdm_adg': 'podbdm_adg.prd.tns',
              'SDW':'psdw_svc.prd.tns',
              'NMOPDB':' pmopdb_svc.prd.tns',
              'EDW_ACCEPT': 'aedw_svc.acc.tns',
              'EDW_PROD': 'pedw_svc.prd.tns',
              ## DRMDB
              'drmdb_prd': 'drm.prd.tns',
              'ADRMDB': 'adrm_svc.acc.tns',
              'PDRMDB': 'pdrm_svc.prd.tns',
              'RICO_PROD': 'prico_svc.prd.tns',
              'ECAF_PROD': 'pecaf_svc.prd.tns',
              'ANEP': 'anep_svc.acc.tns'
             }








