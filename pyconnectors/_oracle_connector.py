"""
Created on Tue Mar 17 00:16:01 2020

@author: mingare

"""
import pandas as pd
import os, ntpath
from _PATHS import USER_GUIDE_URL

# Define Generic Oracle Connector    
class Oracle_Connector:
    
    def __init__(self, user=None, pwd=None, dsn=None, encoding='UTF-8'):
        self.user = user
        self.pwd = pwd
        self.dsn = dsn
        if os.name=='nt':     # LOCAL - Windows
            import pyodbc
            self._cnxn = pyodbc.connect(f'DSN={dsn};UID={user.upper()};PWD={pwd}')

        else:                 # CDSW (os.name='POSIX')
            import cx_Oracle
#            cx_Oracle.init_oracle_client('/home/cdsw/.local/lib/python3.6/site-packages/pyoracleclient/instantclient')
            self._cnxn = cx_Oracle.connect(user, pwd, dsn, encoding=encoding)
        self._cursor = self._cnxn.cursor()
        
        try:    # This works on jupyter ipython
            self._is_jupyter = bool(get_ipython().config)
            self._is_ipython = True
        except: # On plain python get_ipython is not defined
            self._is_jupyter = self._is_ipython =  False

        
        
    def get_tables(self, ret=True):
        """Prints and returs tables within current database."""
        self._cursor.execute("select OWNER, TABLE_NAME from ALL_TABLES")
        tables, = zip(*self._cursor.fetchall())
        if ret:
            return tables
        else:
            for table in tables:
                print(table)
            
    def describe_table(self, table):
        """Describes a table and returns DataFrame with description.
        `table` argument can be provided as `name` or as `owner.name`."""
        if '.' in table:
            ownr = table.split('.')[0]
        else:
            tbls = self.read_sql('select OWNER, TABLE_NAME from ALL_TABLES')
            ownr = tbls[tbls.TABLE_NAME==table].OWNER.iloc[0]
        self._cursor.execute(f'select * from {ownr}.{table} where 1=0')
        columns, dtype, _, length, *_ = [*zip(*self._cursor.description)]
        df = pd.DataFrame({'columns': columns, 
                           'dtype': [tp.name for tp in dtype]})
        return df
    
    def read_sql(self, query, **kwargs):
        """Performs a SQL query.
        Args: 
            query (str): a SQL query.
            kwargs:      Keyword arguments to be passed to pandas.read_sql.
        Returns: 
            pd.DataFrame
        """
        return pd.read_sql(query, con=self._cnxn, **kwargs)
    
    
    def __repr__(self):
        states = ('Not active', 'Active')
        sb, eb = ("\033[1m","\033[0;0m") if self._is_ipython else ('','')
        sr, er = ("\x1b[31m","\x1b[0m") if self._is_ipython else ('','')
        sg, eg = ("\033[92m","\033[0m") if self._is_ipython else ('','')
        clr_str = lambda isc:(sg,eg) if isc else (sr,er)
        state_str = lambda isc:  f'{clr_str(isc)[0]}{states[isc]}{clr_str(isc)[1]}'
        _repr = f"{sb}Oracle connection{eb}: {state_str(True)}"\
                f"\nSelected database: {self.dsn}"
        return _repr

    
    def _repr_html_(self):
        with open(f'{os.path.dirname(__file__)}/res/connector_mini.svg', 'r') as f: 
            _svg_cnn = f.read()
        states = ('Not active', 'Active')
        colors = ('#C82806', '#138F0B')
        bcolors = ('#FCD9D9', '#DBFCD9')
        html_repr = _svg_cnn + f"""</br>
        <span style="white-space: nowrap;">
        <b>Oracle connection</b>:
        <span style="color:{colors[1]}; 
                     background-color:{bcolors[1]}"; 
        white-space: nowrap;>{states[1]}</span>
        </span></br>
        <span style="white-space: nowrap;">
        <span style="color: gray">Selected database:</span>
        <span white-space: nowrap;>{self.dsn}</span>
        </span>

        </br></br>
        <a href="{USER_GUIDE_URL}">
        Need help? Check the documentation!</a>"""

        return html_repr
        
    
    def __del__(self):
        try:
            self._cursor.close()
            self._cnxn.close()
        except:
            pass
            
