# CONTRIBUTING

TODO: define template for contribution and detail
procedure to update 

1. Always test the whole package when modifying the tool
2. Always introduce new tests for new features

# Versioning

1. Version needs to be changed in both `connectors.__about__` and in `README.md`.
2. Commit as `git commit -m 'vx.y.z - commit_message here'`
3. Always tage new versions, e.g. as `git tag -a v3.0.0 -m "Version 3.0.0"` (after commiting)
