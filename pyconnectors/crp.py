"""
Created on Tue Mar 17 00:11:21 2020

@author: mingare

"""
import os, functools
from pyconnectors._oracle_connector import Oracle_Connector

# Define CRP Connector
CRP = functools.partial(Oracle_Connector, dsn="CRP")

try:
    crp = CRP(user=os.environ['CRP_USER'], 
              pwd=os.environ['CRP_PWD'])
except:
    raise Warning("""Incorrect credentials. 
    Please either set the correct credentials in the environmental variables
    
    user=os.environ['CRP_USER']
    pwd=os.environ['CRP_PWD']
    
    or instantiate as 
    
    crp = CRP(user=<your-user>, pwd=<your-pwd>)""")
