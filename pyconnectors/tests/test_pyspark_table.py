from pyconnectors import disc
disc.connect_spark()
ICG_PATH = '/data/lab/dlb_disc_icg/db'

df = disc.spark.sql("""SELECT * FROM lab_dlb_disc_icg.pd_cty_avrgs""")


disc.create_table(df, lab='lab_dlb_disc_icg', table_name='adlsfbwliurw', 
                  path=f'{ICG_PATH}/TABLES/example_table_delete_at_will')

df.write.parquet(f'{ICG_PATH}/TABLES/example_table_delete_at_will')

disc._hdfs_cnxn.chmod(f'{ICG_PATH}/TABLES/example_table_delete_at_will', '777')


file = [f for f in disc.ls(f'{ICG_PATH}/TABLES/example_table_delete_at_will') if f[-8:]=='.parquet'][0]

query = (f"""CREATE EXTERNAL TABLE lab_dlb_disc_icg.wellallalala
             LIKE PARQUET '{file}' 
             STORED AS PARQUET 
             LOCATION '{ICG_PATH}/TABLES/example_table_delete_at_will';""")
disc._cursor.execute(query)