import pandas as pd
from pyconnectors import disc
import os 
user_name = os.environ['HADOOP_USER_NAME']
disc.get_databases()
disc.select_database('crp_gleif')
disc.get_tables()
disc.describe_table('gleif_lei2_cdf_public_records')

df = disc.read_sql(f"""SELECT DISTINCT(lei), hq_city
                     FROM crp_gleif.gleif_lei2_cdf_public_records
                     WHERE hq_city = 'London'
                  """)
print(df)
disc.listdir(f'/user/{user_name}/', full_path=False)


df = pd.DataFrame({'A': [1,2,3],
                   'B': ['one', 'one', 'four']})
# Replace path with you username, or desired location
disc.to_csv(df=df, path=f'/user/{user_name}/table.csv')  
disc.read_csv(f'/user/{user_name}/table.csv')


disc.to_parquet(df=df, path=f'/user/{user_name}/table.parq')  
disc.read_parquet(f'/user/{user_name}/table.parq')


## TEST ENCRYPTION    
disc.to_encrypted(df, f'/user/{user_name}/table.encrypted', password='pippo')
disc.read_encrypted(f'/user/{user_name}/table.encrypted', password='pippo')


disc._cnxn    # Impala connection

