# TEST ORACLE
#from pyconnectors import fssdb
import os
import pyoracleclient as pyoc
print(pyoc.__file__)

from pyconnectors import Oracle_Connector

fssdb = Oracle_Connector(dsn="FSSDB",
                         user=os.environ['FSSDB_USER'], 
                         pwd=os.environ['FSSDB_PWD'])