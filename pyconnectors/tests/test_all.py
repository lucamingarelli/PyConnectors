import pandas as pd, numpy as np
from pyconnectors import disc
import os
user_name = os.environ['HADOOP_USER_NAME']

df = pd.DataFrame({'A': [1,2,3],
                   'B': ['one', 'one', 'four']})

def test_all():
    COUNTER = 0
    # ------------------- get_database -------------------
    try:
        disc.get_databases(ret=True);
    except:
        print('FAILED on disc.get_databases()')
        COUNTER += 1
    # ------------------- select_database -------------------
    try:
        disc.select_database('crp_gleif');
    except:
        print("FAILED on disc.select_database('crp_gleif')")
        COUNTER += 1
    # ------------------- get_tables -------------------
    try:
        disc.get_tables();
    except:
        print("FAILED on disc.get_tables()")
        COUNTER += 1
    # ------------------- describe_table -------------------
    try:
        disc.describe_table('gleif_lei2_cdf_public_records');
    except:
        print("FAILED on disc.describe_table('gleif_lei2_cdf_public_records')")    
        COUNTER += 1
    # ------------------- pd.read_sql -------------------
    try:
        df = pd.read_sql(f"""SELECT DISTINCT(lei), hq_city
                         FROM crp_gleif.gleif_lei2_cdf_public_records
                         WHERE hq_city = 'London'
                          """, con=disc._cnxn)
    except:
        print("FAILED on pd.read_sql")
        COUNTER += 1
    # ------------------- disc.listdir -------------------    
    try:
        disc.listdir('/user/mingare', full_path=False);
    except:
        print("FAILED on disc.listdir('/user/mingare', full_path=False)")  
        COUNTER += 1
    # ------------------- disc.to_csv -------------------    
    try:
        # Replace path with you username, or desired location
        disc.to_csv(df=df, path='/user/mingare/test_table.csv')  
    except:
        print("FAILED on disc.to_csv")  
        COUNTER += 1
    # ------------------- disc.read_csv -------------------        
    try:
        df2 = disc.read_csv('/user/mingare/test_table.csv')
    except:
        print("FAILED on disc.read_csv")
        COUNTER += 1
    # ------------------- disc._cnxn -------------------    
    try:
        disc._cnxn    # Impala connection
    except:
        print("FAILED on disc._cnxn")
        COUNTER += 1
    # ------------------- disc._hdfs_cnxn -------------------    
    try:
        disc._hdfs_cnxn  # Pyarrow connection
    except:
        print("FAILED on disc._hdfs_cnxn")
        COUNTER += 1
    # ------------------- disc.to_parquet -------------------    
    try:
        disc.to_parquet(df, '/user/mingare/test_table.parq')
    except:
        print("FAILED on disc.to_parquet")  
        COUNTER += 1
    # ------------------- disc.read_parquet -------------------    
    try:
        df2 = disc.read_parquet('/user/mingare/test_table.parq')
    except:
        print("FAILED on disc.read_parquet")  
        COUNTER += 1
    # ------------------- disc.to_feather -------------------    
    try:
        disc.to_feather(df, '/user/mingare/test_table.feather')
    except:
        print("FAILED on disc.to_feather")  
        COUNTER += 1
    # ------------------- disc.read_feather -------------------    
    try:
        df2 = disc.read_feather('/user/mingare/test_table.feather')
    except:
        print("FAILED on disc.read_feather") 
        COUNTER += 1
    # ------------------- disc.to_pickle -------------------
    try:
        disc.to_pickle(df, '/user/mingare/test_table.p');
    except:
        print('FAILED on disc.to_pickle()')
        COUNTER += 1
    # ------------------- disc.read_pickle -------------------
    try:
        df2 = disc.read_pickle('/user/mingare/test_table.p');
    except:
        print('FAILED on disc.read_pickle()')
        COUNTER += 1

    # ------------------- disc.savefig -------------------
    try:
        def plot_and_save():
            df.plot()
            disc.savefig('/user/mingare/fig.png', dpi=100)
        plot_and_save()
    except:
        print('FAILED on disc.savefig()')
        COUNTER += 1

    # ------------------- disc.to_encrypted -------------------
    try:
        disc.to_encrypted(df, '/user/mingare/table.encrypted', 
                          password='pippo')

    except:
        print('FAILED on disc.to_encrypted()')
        COUNTER += 1
    
    # ------------------- disc.read_encrypted -------------------
    try:
        disc.read_encrypted('/user/mingare/table.encrypted', 
                            password='pippo')

    except:
        print('FAILED on disc.read_encrypted()')
        COUNTER += 1

    # ------------------- disc.to_stata -------------------
    try:
        disc.to_stata(df, '/user/mingare/table.dta')

    except:
        print('FAILED on disc.to_stata()')
        COUNTER += 1
    
    # ------------------- disc.read_stata -------------------
    try:
        disc.read_stata('/user/mingare/table.dta')

    except:
        print('FAILED on disc.read_stata()')
        COUNTER += 1
    
    # ------------------- disc.create_table -------------------
    try:
        disc.create_table(df, lab='lab_dlb_ecb_public', 
                      table_name='My_new_table',
                      path='/data/lab/dlb_ecb_public/db') 

    except:
        print('FAILED on disc.create_table()')
        COUNTER += 1
        
        
        
    
        
    # ------------------- disc._hdfs_cnxn.rm -------------------    
    try:
        disc._hdfs_cnxn.rm('/user/mingare/test_table.csv')
        disc._hdfs_cnxn.rm('/user/mingare/test_table.parq')
        disc._hdfs_cnxn.rm('/user/mingare/test_table.feather')
    except:
        print("FAILED on disc._hdfs_cnxn.rm")  
        COUNTER += 1

    
    print(f"\n----------\n\n{COUNTER} tests have failed.")
    print("All others passed.")
    
if __name__=='__main__':
    test_all()
    