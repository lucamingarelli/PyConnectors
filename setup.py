import os, setuptools
IS_WINDOWS =  os.name=='nt'

with open("README.md", "r") as f:
    long_description = f.read()
    
about = {}
with open("pyconnectors/__about__.py") as f:
    exec(f.read(), about)

    
if not IS_WINDOWS:
    os.system('pip install pyoracleclient==0.1.40')
    import pyoracleclient as pyoc
    pyoc.get_client(version='19.3.0.0.0', sys='linux')
    pyoc.delete_all_tns(confirm=True)
    from pyconnectors.oracle_databases import ORACLE_DBS, HOSTS, PORTS

    for name, service_name in ORACLE_DBS.items():
        pyoc.add_tns(name=name, service_name=service_name,
                     protocol1='TCP', host1=HOSTS[0], port1=PORTS[0],
                     protocol2='TCP', host2=HOSTS[1], port2=PORTS[1])

install_requirements = ['pandas', 'cryptpandas', 'matplotlib',
                        'fsspec','requests','requests_kerberos']
if IS_WINDOWS:
    install_requirements += ['pyodbc']
else:
    install_requirements += ['pyarrow==0.15.0','cx_oracle', 'pyoracleclient']

setuptools.setup(
    name="ecb_connectors",
    version=about['__version__'], 
    author=about['__author__'],
    author_email=about['__email__'],
    description=about['__about__'],
    url=about['__url__'],
    license='MIT',
    long_description=long_description,
    long_description_content_type="markdown",
    packages=setuptools.find_packages(),
    include_package_data=True,
    package_data={'':  ['../pyconnectors/certificates/*.pem','../pyconnectors/res/*' ]},
    install_requires=install_requirements,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)



# Log installation
try:
    from pyconnectors import disc
    import datetime
    _path = '/data/lab/dlb_ecb_public/share/_CONNECTORS_LOG'
    date = str(datetime.datetime.today().date())
    if f'{date}_install' not in disc.listdir(_path):
        disc.to_pickle(0, f'{_path}/{date}_install/instl_logs.p')
    else:
        L = disc.read_pickle(f'{_path}/{date}_install/instl_logs.p')
        disc.to_pickle(L+1, f'{_path}/{date}_install/instl_logs.p')
    print('Done.')
except:
    pass




